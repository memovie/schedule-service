package id.phully.schedule.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Field;

@Setter
@Getter
@NoArgsConstructor
public class Schedule {

    private String type;

    private String price;

    @Field("showtime")
    private String[] showTimes;

    public Schedule(String type, String price, String[] showTimes) {
        this.type = type;
        this.price = price;
        this.showTimes = showTimes;
    }
}
