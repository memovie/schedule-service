package id.phully.schedule.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@ToString
public class Show {
    private String cinema;

    private String cinemaThumbnail;

    @Field("schedule")
    private List<Schedule> schedules;

    public Show(String cinema, String cinemaThumbnail, List<Schedule> schedules) {
        this.cinema = cinema;
        this.cinemaThumbnail = cinemaThumbnail;
        this.schedules = schedules;
    }
}
