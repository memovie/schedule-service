package id.phully.schedule.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.List;

@Document(collection = "schedules")
@Setter
@Getter
@NoArgsConstructor
@ToString(exclude = "_id")
public class Movie implements Serializable {
    @Id
    public String _id;

    private String title;

    private String cover;

    private String location;

    @Field("show")
    private List<Show> shows;

    public Movie(String _id, String title, String cover, String location, List<Show> shows) {
        this._id = _id;
        this.title = title;
        this.cover = cover;
        this.location = location;
        this.shows = shows;
    }
}
