package id.phully.schedule.controllers;

import id.phully.schedule.models.Movie;
import id.phully.schedule.repositories.FIlmRepository;
import id.phully.schedule.utilities.exceptions.ResourceNotFoundException;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/public/")
public class PublicController {

    @Autowired
    private FIlmRepository fIlmRepository;

    @GetMapping({"/movie/{scheduleQueryTitle}"})
    public Movie getAllSchedule (
            @PathVariable ObjectId scheduleQueryTitle
    ){
        return fIlmRepository.findById(scheduleQueryTitle).orElseThrow(ResourceNotFoundException::new);
    }

    @GetMapping({"/playing/{city}"})
    public Page<Movie> getAllCityPlaying(
            @PathVariable String city,
            Pageable pageable
    ) {
        if(fIlmRepository.existsByLocation(city)){
            return fIlmRepository.findMoviesByLocation(city, pageable);
        } else{
            throw new ResourceNotFoundException();
        }
    }

}
