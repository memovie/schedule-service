package id.phully.schedule.repositories;

import id.phully.schedule.models.Movie;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FIlmRepository extends MongoRepository<Movie, ObjectId> {
    Page<Movie> findMoviesByLocation(String locationQuery, Pageable pageable);
    Boolean existsByLocation(String locationQuery);
}
